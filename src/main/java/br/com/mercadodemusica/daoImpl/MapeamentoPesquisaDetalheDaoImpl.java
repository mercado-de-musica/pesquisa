package br.com.mercadodemusica.daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.MapeamentoPesquisaDetalheDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.MapeamentoPesquisaDetalhe;

@Repository
public class MapeamentoPesquisaDetalheDaoImpl extends DaoImpl<MapeamentoPesquisaDetalhe> implements MapeamentoPesquisaDetalheDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MapeamentoPesquisaDetalheDaoImpl() {
		super(MapeamentoPesquisaDetalhe.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<MapeamentoPesquisaDetalhe> obterMapeamentoPesquisaDetalhePorIdCryptNotIp(String idProdutoCrypt, String ip) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MapeamentoPesquisaDetalhe.class);
		criteria.add(Restrictions.eq("idProdutoCrypt", idProdutoCrypt));
		criteria.add(Restrictions.ne("ip", ip));
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<MapeamentoPesquisaDetalhe> obterMapeamentoPesquisaDetalhePorPesquisaNotIp(MapeamentoPesquisaDetalhe mapeamentoPesquisaDetalhe) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MapeamentoPesquisaDetalhe.class);
		criteria.add(Restrictions.like("pesquisa", "%" + mapeamentoPesquisaDetalhe.getPesquisa() + "%"));
		
		return criteria.list();
	}
}
