package br.com.mercadodemusica.daoImpl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.MapeamentoPesquisaDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.MapeamentoPesquisa;

@Repository
public class MapeamentoPesquisaDaoImpl extends DaoImpl<MapeamentoPesquisa> implements MapeamentoPesquisaDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MapeamentoPesquisaDaoImpl() {
		super(MapeamentoPesquisa.class);
	}

	@Override
	@Transactional(readOnly = true)
	public String obterPesquisaMaisRealizada() {
		Session session = this.sessionFactory.getCurrentSession();
		
		SQLQuery query = session.createSQLQuery("select pesquisa from mapeamento_pesquisa group by pesquisa order by count(pesquisa) desc limit 1");
		
		return (String) query.uniqueResult();		
	}

}
