package br.com.mercadodemusica.serviceImpl;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dozer.Mapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mercadodemusica.acessoHttp.HTTPRequestResponse;
import br.com.mercadodemusica.dao.MapeamentoPesquisaDao;
import br.com.mercadodemusica.dao.MapeamentoPesquisaDetalheDao;
import br.com.mercadodemusica.dto.ApresentacaoProdutoDTO;
import br.com.mercadodemusica.dto.MapeamentoPesquisaDTO;
import br.com.mercadodemusica.dto.MapeamentoPesquisaDetalheDTO;
import br.com.mercadodemusica.dto.PrecoDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.entities.MapeamentoPesquisa;
import br.com.mercadodemusica.entities.MapeamentoPesquisaDetalhe;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.EncryptConstantes;
import br.com.mercadodemusica.properties.UrlModulo;
import br.com.mercadodemusica.service.MapeamentoPesquisaService;
import br.com.mercadodemusica.transformacaoDeDados.Encrypt;

@Service
public class MapeamentoPesquisaServiceImpl implements MapeamentoPesquisaService {

	@Autowired
	private MapeamentoPesquisaDao mapeamentoPesquisaDao;
	
	@Autowired
	private MapeamentoPesquisaDetalheDao mapeamentoPesquisaDetalheDao;
	
	@Autowired
	private Mapper dozerBeanMapper;
	
	@Autowired
	private HttpServletRequest request;
	
	private RestTemplate restTemplate = new RestTemplate();
	
	private final Logger logger = LogManager.getLogger(MapeamentoPesquisaServiceImpl.class);
	
	private final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PRODUTO);
	private static UrlModulo urlModuloPedido = new UrlModulo(ModuloSistemaEnum.PEDIDO);
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void salvarPesquisa(MapeamentoPesquisaDTO mapeamentoPesquisaDto) {
		
		if(mapeamentoPesquisaDto.getUsuario() == null || mapeamentoPesquisaDto.getUsuario().getIpUsuario() == null || mapeamentoPesquisaDto.getUsuario().getIpUsuario().isEmpty()) {
			
			logger.warn("sem usuário para a pesquisa: " + dateFormat.format(Calendar.getInstance().getTime()));
			
		} 
		
		if (mapeamentoPesquisaDto.getPesquisa() == null || mapeamentoPesquisaDto.getPesquisa().isEmpty()) {
		
			logger.warn("Sem texto para ser pesquisado" + dateFormat.format(Calendar.getInstance().getTime()));
		
		} else if(mapeamentoPesquisaDto.getPesquisa() != null && !mapeamentoPesquisaDto.getPesquisa().isEmpty() && !mapeamentoPesquisaDto.getPesquisa().equals("null")) {
			
			MapeamentoPesquisa mapeamentoPesquisa = new MapeamentoPesquisa();
			mapeamentoPesquisa.setData(Calendar.getInstance());
			mapeamentoPesquisa.setIp(mapeamentoPesquisaDto.getUsuario().getIpUsuario());
			mapeamentoPesquisa.setPesquisa(mapeamentoPesquisaDto.getPesquisa());
			
			mapeamentoPesquisaDao.save(mapeamentoPesquisa);
			
		}
		
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void salvarPesquisaDetalhe(MapeamentoPesquisaDetalheDTO mapeamentoPesquisaDetalheDto) {
		if(mapeamentoPesquisaDetalheDto.getIp() == null) {
			
			logger.warn("sem ip para a pesquisa: " + dateFormat.format(Calendar.getInstance().getTime()));
			
		} 
		
		if (mapeamentoPesquisaDetalheDto.getPesquisa() == null || mapeamentoPesquisaDetalheDto.getPesquisa().isEmpty()) {
		
			logger.warn("Sem texto para ser pesquisado" + dateFormat.format(Calendar.getInstance().getTime()));
		
		} else {
			
			MapeamentoPesquisaDetalhe mapeamentoPesquisaDetalhe = dozerBeanMapper.map(mapeamentoPesquisaDetalheDto, MapeamentoPesquisaDetalhe.class);	
			mapeamentoPesquisaDetalheDao.save(mapeamentoPesquisaDetalhe);
			
		}
		
	}


	@Override
	public List<ApresentacaoProdutoDTO> obterPesquisasFeitasPelosUsuariosPorProduto(String idProdutoCrypt, String ip) throws ClientProtocolException, URISyntaxException, IOException, RegraDeNegocioException, JSONException {
		
		logger.info("obterPesquisasFeitasPelosUsuariosPorProduto(String idProdutoCrypt, String ip) : " + idProdutoCrypt + " " + ip);
		
		List<ProdutoDTO> listaDeProdutosPesquisados = new ArrayList<ProdutoDTO>();
		List<ApresentacaoProdutoDTO> apresentacoesRetorno = new ArrayList<ApresentacaoProdutoDTO>();
		List<String> listaDeIdCrypt = new ArrayList<String>();
		List<BigInteger> idsApresentacoes = new ArrayList<BigInteger>();
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		List<MapeamentoPesquisaDetalhe> listaMapeamentoPesquisaDetalhe =  mapeamentoPesquisaDetalheDao.obterMapeamentoPesquisaDetalhePorIdCryptNotIp(idProdutoCrypt, ip);
		for(MapeamentoPesquisaDetalhe mapeamentoPesquisaDetalhe : listaMapeamentoPesquisaDetalhe) {
			
			List<MapeamentoPesquisaDetalhe> listaNovoMapeamento = mapeamentoPesquisaDetalheDao.obterMapeamentoPesquisaDetalhePorPesquisaNotIp(mapeamentoPesquisaDetalhe);			
			
			for(MapeamentoPesquisaDetalhe novoMapeamento : listaNovoMapeamento) {

				if(!listaDeIdCrypt.contains(novoMapeamento.getIdProdutoCrypt()) && !novoMapeamento.getIdProdutoCrypt().equals(idProdutoCrypt)) {
					listaDeIdCrypt.add(novoMapeamento.getIdProdutoCrypt());
					
					logger.info("urlModuloProduto.getUrlModulo() + \"produto/obterProdutoPorId\", (\"idCrypt=\" + novoMapeamento.getIdProdutoCrypt()");

					String produtoString = (String) HTTPRequestResponse.get(urlModuloProduto.getUrlModulo() + "produto/obterProdutoPorId", ("idCrypt=" + novoMapeamento.getIdProdutoCrypt()),  request.getRemoteAddr());
					ProdutoDTO pesquisaDeProdutosDto = objectMapper.readValue(produtoString, ProdutoDTO.class);
					
					logger.info("pesquisaDeProdutosDto : " + pesquisaDeProdutosDto);
					
					PrecoDTO precoDto = this.buscarPrecoAtual(pesquisaDeProdutosDto);
					
					
					pesquisaDeProdutosDto.setPreco(precoDto.getPreco());
					pesquisaDeProdutosDto.setPrecoString(precoDto.getPrecoString());
					
					listaDeProdutosPesquisados.add(pesquisaDeProdutosDto);
				}
				
			}
			
			
			for(ProdutoDTO produtoDto : listaDeProdutosPesquisados) {
				JSONObject produtoJson = new JSONObject(objectMapper.writeValueAsString(produtoDto));
				produtoJson.remove("descricao");
				
				logger.info("HTTPRequestResponse.get(urlModuloProduto.getUrlModulo() + \"apresentacao/obterlistaDeApresentacaoPorProduto\", (\"produtoDto=\" + produtoJson)");
				
				String listaDeApresentacoesString = (String) HTTPRequestResponse.get(urlModuloProduto.getUrlModulo() + "apresentacao/obterlistaDeApresentacaoPorProduto", ("produtoDto=" + produtoJson), request.getRemoteAddr());
				List<ApresentacaoProdutoDTO> listaDeApresentacoesDto = objectMapper.readValue(listaDeApresentacoesString, new TypeReference<List<ApresentacaoProdutoDTO>>(){});
				
				listaDeApresentacoesDto.get(0).getProduto().setPreco(produtoDto.getPreco());
				listaDeApresentacoesDto.get(0).getProduto().setPrecoString(produtoDto.getPrecoString());
				
				
				listaDeApresentacoesDto.get(0).getProduto().setIdCrypt(Encrypt.encrypt(EncryptConstantes.KEY, EncryptConstantes.INIT_VECTOR, listaDeApresentacoesDto.get(0).getProduto().getId().toString()));
				
				if(! idsApresentacoes.contains(listaDeApresentacoesDto.get(0).getId()) && apresentacoesRetorno.size() < 8) {
					idsApresentacoes.add(listaDeApresentacoesDto.get(0).getId());
					apresentacoesRetorno.add(listaDeApresentacoesDto.get(0));
				}
				
				
			}
		}
		
		
		logger.info("return: " + apresentacoesRetorno);
		return apresentacoesRetorno;
	}

	private PrecoDTO buscarPrecoAtual(ProdutoDTO pesquisaDeProdutosDto) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		logger.info("preco/obterPrecoAtualPorIdProduto/\" + pesquisaDeProdutosDto.getId()");
		ResponseEntity<PrecoDTO> response = restTemplate.exchange(
				urlModuloProduto.getUrlModulo() + "preco/obterPrecoAtualPorIdProduto/" + pesquisaDeProdutosDto.getId(), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<PrecoDTO>() {
				});
		
		return response.getBody();
		
	}


	@Override
	public ProdutoDTO obterPesquisaMaisRealizada() {
		String mapeamentoPesquisaString = mapeamentoPesquisaDao.obterPesquisaMaisRealizada();
		ProdutoDTO produtoDto = new ProdutoDTO();
		produtoDto.setNome(mapeamentoPesquisaString);
		
		
		return produtoDto;
	}
}
