package br.com.mercadodemusica.restController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mercadodemusica.dto.ApresentacaoProdutoDTO;
import br.com.mercadodemusica.dto.MapeamentoPesquisaDTO;
import br.com.mercadodemusica.dto.MapeamentoPesquisaDetalheDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.exceptions.ProdutoException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.service.MapeamentoPesquisaService;

@RestController
@CrossOrigin
@RequestMapping(value = "/mapeamentoPesquisa")
public class MapeamentoPesquisaRestController {

	@Autowired
	private MapeamentoPesquisaService mapeamentoPesquisaService;
	
	private final Logger logger = LogManager.getLogger(MapeamentoPesquisaRestController.class);
	
	@RequestMapping(value = "/salvarPesquisa",  method = RequestMethod.POST)
	public void salvarPesquisa(@RequestBody MapeamentoPesquisaDTO mapeamentoPesquisaDto) {
		
		
		mapeamentoPesquisaService.salvarPesquisa(mapeamentoPesquisaDto);

	}
	
	@RequestMapping(value = "/salvarPesquisaDetalhe",  method = RequestMethod.POST)
	public void salvarPesquisa(@RequestBody MapeamentoPesquisaDetalheDTO mapeamentoPesquisaDetalheDto) {
		mapeamentoPesquisaService.salvarPesquisaDetalhe(mapeamentoPesquisaDetalheDto);
	}
	
	
	@RequestMapping(value = "/obterPesquisasFeitasPelosUsuariosPorProduto/{idProdutoCrypt}/{ip}")
	public List<ApresentacaoProdutoDTO> obterPesquisasFeitasPelosUsuariosPorProduto(@PathVariable("idProdutoCrypt") String idProdutoCrypt, @PathVariable("ip") String ip) throws ProdutoException, ClientProtocolException, URISyntaxException, IOException, RegraDeNegocioException, JSONException {
		
		logger.warn("metodo: " + "obterPesquisasFeitasPelosUsuariosPorProduto/{idProdutoCrypt}/{ip}");
		
		return mapeamentoPesquisaService.obterPesquisasFeitasPelosUsuariosPorProduto(idProdutoCrypt, ip);
		
	}

	@RequestMapping(value = "/obterPesquisaMaisRealizada")
	public ProdutoDTO obterPesquisaMaisRealizada() throws ProdutoException, ClientProtocolException, URISyntaxException, IOException, RegraDeNegocioException, JSONException {
		
		logger.warn("metodo: " + "obterPesquisaMaisRealizada()");
		
		return mapeamentoPesquisaService.obterPesquisaMaisRealizada();
		
	}
}
