package br.com.mercadodemusica.dao;

import java.util.List;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.MapeamentoPesquisaDetalhe;

public interface MapeamentoPesquisaDetalheDao extends Dao<MapeamentoPesquisaDetalhe> {

	List<MapeamentoPesquisaDetalhe> obterMapeamentoPesquisaDetalhePorIdCryptNotIp(String idProdutoCrypt, String ip);

	List<MapeamentoPesquisaDetalhe> obterMapeamentoPesquisaDetalhePorPesquisaNotIp(MapeamentoPesquisaDetalhe mapeamentoPesquisaDetalhe);

}
