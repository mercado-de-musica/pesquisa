package br.com.mercadodemusica.dao;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.MapeamentoPesquisa;

public interface MapeamentoPesquisaDao extends Dao<MapeamentoPesquisa> {

	String obterPesquisaMaisRealizada();

}
