package br.com.mercadodemusica.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import br.com.mercadodemusica.dto.ApresentacaoProdutoDTO;
import br.com.mercadodemusica.dto.MapeamentoPesquisaDTO;
import br.com.mercadodemusica.dto.MapeamentoPesquisaDetalheDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;

public interface MapeamentoPesquisaService {

	void salvarPesquisa(MapeamentoPesquisaDTO mapeamentoPesquisaDto);

	void salvarPesquisaDetalhe(MapeamentoPesquisaDetalheDTO mapeamentoPesquisaDetalheDto);

	List<ApresentacaoProdutoDTO> obterPesquisasFeitasPelosUsuariosPorProduto(String idProdutoCrypt, String ip) throws ClientProtocolException, URISyntaxException, IOException, RegraDeNegocioException, JSONException;

	ProdutoDTO obterPesquisaMaisRealizada();

}
