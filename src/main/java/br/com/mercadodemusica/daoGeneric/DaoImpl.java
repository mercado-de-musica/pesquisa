package br.com.mercadodemusica.daoGeneric;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public abstract class DaoImpl<T> implements Dao<T>, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	protected SessionFactory sessionFactory;

	private Class<T> entityClass;

	public DaoImpl(Class<T> clazzToSet) {
		this.entityClass = clazzToSet;
	}

	public Class<T> getEntityClass() {
		return entityClass;
	}

	@Override
	public BigInteger save(T t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(t);
		return (BigInteger) session.getIdentifier(t);
	}

	@Override
	public void delete(T t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(t);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<T> selectAll() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(entityClass);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public T selectOne(BigInteger id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(entityClass);
		criteria.add(Restrictions.eq("id", id));
		criteria.setMaxResults(1);

		return (T) criteria.uniqueResult();

	}

	@Override
	@Transactional(readOnly = true)
	public Boolean contains(T t) {
		Session session = this.sessionFactory.getCurrentSession();
		return session.contains(t);
	}

	@Override
	@Transactional(readOnly = true)
	public void refresh(T t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.refresh(t);
	}
}	


