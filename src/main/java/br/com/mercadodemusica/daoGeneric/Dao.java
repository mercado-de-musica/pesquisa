package br.com.mercadodemusica.daoGeneric;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

public interface Dao<T> extends Serializable {
	
	BigInteger save(T t);

	void delete(T t);

	public List<T> selectAll();

	public T selectOne(BigInteger id);

	Boolean contains(T t);
	
	void refresh(T t);
}
